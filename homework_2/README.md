## Computer graphics

Author      : Moris Pozzati  
Mat number  : 073344  
Description : homework 2  
Date        : 2018/11/28  

### Exercise 1:
Partendo dal template fornito in consegna sono stati aggiunte le seguenti **funzioni** per costruire i corrispettivi solidi geometrici:
- *drawCone*: costruisce un cono facendo ruotare un triangolo rettangolo sull'asse Y  
- *drawCylinder*: costruisce un cilindro facendo ruotare un rettangolo sull'asse Y  
- *drawSphere*: costruisce una sfera di raggio R ruotando lo stesso di 180 gradi sull'asse z e creando una serie di circonferenze di raggio r pari al R per il seno dell'angolo creato tra l'asse Z ed R.  
- *drawTorus*: costruisce un toro facendo ruotare sul piano xy una circonferenza di raggio R una seconda circonferenza di raggio r (circoferenza su z)  

### Exercise 2
Il solido scelto per l'applicazione del modello di Blinn-Phong è il **toro**. Rispetto all'esercizio precedente, al Vertex Shader non sarà piu passato l'array contenete il colore dei vertici ma quello delle **normali** e delle componenti di luce e materiali (questi ultimi deviniti partendo dalla tabella della consegna). Le normali sono quindi calcolate per ogni vertice.
La normale per ogni vertice *p* del toro è diretta dal centro del cerchio definito dalla circonferenza di raggio r a cui appartiene *p* verso l’esterno e la direzione è quella del raggio che unisce il centro al punto *p*.  


### Exercise 3
Come richiesto dalla consegna nel terzo esercizio l'implementazione di Blinn Phong è stata spostata nel Fragment shader (FS).
I dati al FS possono essere passati solamente dal Vertex Shader (VS) definendo delle variabili **varying**.
Nel VS è rimasta la parte di definizione di gl_Position e si è aggiunta quella di assegnamento alle variabili varying che definiscono componenti della luce, componenti del materiale e normali dei vertici.
Nel FS è stato spostato il codice dal VS mantenendolo praticamente inalterato se non con la sola modifica del tipo di variabili utilizzate: non più *uniform* bensì *varying*  

Si può notare un miglioramento della qualità del solido con l'implementazione di Blinn Phong nel FS. Miglioramento che è piu marcato al dimnuire dei triangoli che compongono la scena.
