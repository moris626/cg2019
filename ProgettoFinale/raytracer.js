/******************************************************************************
Author      : Moris Pozzati
Mat number  : 073344
Description : Final Project
Date        : 2019/01/16
*******************************************************************************/

//CORE VARIABLES
var canvas;
var context;
var imageBuffer;

var DEBUG = false; //whether to show debug messages
var EPSILON = 0.0001; //error margins

//scene to render
var scene;
var camera;
var materials = [];
var surfaces = [];
var lights;

//initializes the canvas and drawing buffers
function init() {
  canvas = $('#canvas')[0];
  context = canvas.getContext("2d");
  imageBuffer = context.createImageData(canvas.width, canvas.height); //buffer for pixels
  //loadSceneFile("assets/SphereTest.json");
  //loadSceneFile("assets/TriangleTest.json");
  //loadSceneFile("assets/SphereShadingTest1.json");
  //loadSceneFile("assets/SphereShadingTest2.json");
  //loadSceneFile("assets/TriangleShadingTest.json");
  //loadSceneFile("assets/TransformationTest.json");
  loadSceneFile("assets/FullTest.json");
  //loadSceneFile("assets/CornellBox.json");
  //loadSceneFile("assets/RecursiveTest.json");
  //loadSceneFile("assets/ShadowTest1.json");
  //loadSceneFile("assets/ShadowTest2.json");

}

//loads and "parses" the scene file at the given path
function loadSceneFile(filepath) {
  scene = Utils.loadJSON(filepath); //load the scene
  // Set up camera
  camera = new Camera(scene.camera.eye,
                      scene.camera.at,
                      scene.camera.up,
                      scene.camera.fovy,
                      scene.camera.aspect);

  // Set up lights
  lights = new Lights(scene.lights);

  // Set up materials
  materials = [];
  for (var i = 0; i < scene.materials.length; i++) {
    materials.push(new Material(scene.materials[i]));
  }
  // Set up surfaces
  surfaces = [];
  for (var i = 0; i < scene.surfaces.length; i++) {
      if (scene.surfaces[i].shape === 'Sphere' ){
        console.log('Loading Sphere');
        surfaces.push(new Sphere(
          scene.surfaces[i].center,
          scene.surfaces[i].radius,
          materials[scene.surfaces[i].material],
          scene.surfaces[i].transforms !== undefined ? scene.surfaces[i].transforms : []
        ));
      }
      if (scene.surfaces[i].shape === 'Triangle' ){
        console.log('Loading Sphere');
        surfaces.push(new Triangle(
          scene.surfaces[i].p1,
          scene.surfaces[i].p2,
          scene.surfaces[i].p3,
          materials[scene.surfaces[i].material],
          scene.surfaces[i].transforms !== undefined ? scene.surfaces[i].transforms : []
        ));
      }
  }
  EPSILON = scene.shadow_bias;
  render(); //render the scene

}

//renders the scene
function render() {
  var start = Date.now(); //for logging
  var firstsurfaceindex,p,n;
  var t,tbest;
  var raybest,s_ray,ray;
  for(var i = 0; i < canvas.width; i++){
    for (var j = 0; j < canvas.height; j++) {
      tbest = Number.POSITIVE_INFINITY;
      firstsurfaceindex = null;
      ray = camera.castRay(i,j);
      for (var k = 0; k < surfaces.length; k++) {
        [s_ray, t]= surfaces[k].intersects(ray,0,tbest);
        if (t>=0) {
          firstsurfaceindex = k;
          tbest = t;
          raybest = s_ray;
        }
      }
      if(firstsurfaceindex == null){
        //background
        setPixel(i, j, [0.0,0.0,0.0]);
      }else{
        //compute shading
        p = raybest.point_at(tbest);
        n = surfaces[firstsurfaceindex].getNormal(p);
        var pp = glMatrix.vec4.create();
        pp[0] = p[0];
        pp[1] = p[1];
        pp[2] = p[2];
        pp[3] = 1;
        glMatrix.vec4.transformMat4(pp,pp,surfaces[firstsurfaceindex].transformMatrix.mat);
        setPixel(i, j, surfaces[firstsurfaceindex].shade(camera.eye,pp,n,lights,surfaces,scene.bounce_depth !== undefined ? scene.bounce_depth : 0));
      }
    }
  }
  //render the pixels that have been set
  context.putImageData(imageBuffer,0,0);

  var end = Date.now(); //for logging
  $('#log').html("rendered in: "+(end-start)+"ms");
  console.log("rendered in: "+(end-start)+"ms");

}

//sets the pixel at the given x,y to the given color
/**
 * Sets the pixel at the given screen coordinates to the given color
 * @param {int} x     The x-coordinate of the pixel
 * @param {int} y     The y-coordinate of the pixel
 * @param {float[3]} color A length-3 array (or a vec3) representing the color. Color values should floating point values between 0 and 1
 */
function setPixel(x, y, color){
  var i = (y*imageBuffer.width + x)*4;
  imageBuffer.data[i] = (color[0]*255) | 0;
  imageBuffer.data[i+1] = (color[1]*255) | 0;
  imageBuffer.data[i+2] = (color[2]*255) | 0;
  imageBuffer.data[i+3] = 255; //(color[3]*255) | 0; //switch to include transparency
}

//converts degrees to radians
function rad(degrees){
  return degrees*Math.PI/180;
}

//on load, run the application
$(document).ready(function(){
  init();
  //render();

  //load and render new scene
  $('#load_scene_button').click(function(){
    var filepath = 'assets/'+$('#scene_file_input').val()+'.json';
    loadSceneFile(filepath);
  });

  //debugging - cast a ray through the clicked pixel with DEBUG messaging on
  $('#canvas').click(function(e){
    var x = e.pageX - $('#canvas').offset().left;
    var y = e.pageY - $('#canvas').offset().top;
    DEBUG = true;
    //camera.castRay(x,y); //cast a ray through the point
    ray = camera.castRay(339,174);
    [s_ray, t] = surfaces[0].intersects(ray,0,Number.POSITIVE_INFINITY);
    p = s_ray.point_at(t);

    var pp = glMatrix.vec4.create();
    pp[0] = p[0];
    pp[1] = p[1];
    pp[2] = p[2];
    pp[3] = 1;
    glMatrix.vec4.transformMat4(pp,pp,surfaces[0].transformMatrix.mat);
    console.log("normal");
    n = surfaces[0].getNormal(pp);
    console.log(n);
    console.log("shade");
    setPixel(x, y, surfaces[0].shade(camera.eye,pp,n,lights,surfaces,scene.bounce_depth !== undefined ? scene.bounce_depth : 0));
    DEBUG = false;
  });

});

/**
 * Create a new Camera:
 * compute sdr_u,sdr_v,sdr_w vectors
 * and scene position letf,right,top,bottom
 *
 * @param {vec3} a eye vector
 * @param {vec3} a at vector
 * @param {vec3} a up vector
 * @param {float} a field of view
 * @param {float} aspect ratio
*/
var Camera = function(eye,at,up,fovy,aspect){
  this.eye = eye;
  this.at = at;
  this.up = up;
  this.fovy = fovy
  this.aspect = aspect;

  // compute origin ("e" vector) in world coordinates - VIEW MATRIX
  this.sdr_w = glMatrix.vec3.create();
  this.sdr_u = glMatrix.vec3.create();
  this.sdr_v = glMatrix.vec3.create();
  // vector w
  glMatrix.vec3.subtract(this.sdr_w,this.eye,this.at);
  glMatrix.vec3.normalize(this.sdr_w,this.sdr_w);
  // vector u
  glMatrix.vec3.cross(this.sdr_u,this.up,this.sdr_w)
  glMatrix.vec3.normalize(this.sdr_u,this.sdr_u);
  // vector v
  glMatrix.vec3.cross(this.sdr_v,this.sdr_w,this.sdr_u);


  this.h = 2 * Math.tan(rad(this.fovy/2.0));
  this.w = this.h * this.aspect;

  this.l = (this.w / (canvas.width -1)) - this.w / 2.0;
  this.r = (this.w * (canvas.width -1) / (canvas.width -1)) - this.w / 2.0;
  this.t = (-this.h * (canvas.height -1) / (canvas.height -1)) + this.h / 2.0;
  this.b = (-this.h / (canvas.height -1)) + this.h / 2.0;

  if (DEBUG) {
    console.log("*** SdR CAM ***");
    console.log("w:"+this.sdr_w);
    console.log("v:"+this.sdr_v);
    console.log("u:"+this.sdr_u);
    console.log("***************");
  }
}

/**
 * Ray generation which computes the origin and direction
 * of each pixel’s viewing ray based on the camera geometry;
 *
 * Mathematical representation for a ray.
 * A ray is really just an origin point and a propagation direction;
 * a 3D parametric line is ideal for this.
 *
 * @param {Number} x coord on uv plain
 * @param {Number} y coord on uv plain
 * @returns {Ray} observer ray from viewpoint "p" to direction "d" on uv plain
*/
Camera.prototype.castRay = function(x,y){
  var vec3temp = glMatrix.vec3.create();
  var uu = glMatrix.vec3.create();
  var vv = glMatrix.vec3.create();
  var u,v;
  // calcolo coordinate u,v
  u = this.l + ( this.r - this.l ) * ( x + 0.5 ) / canvas.width;
  v = this.b + ( this.t - this.b ) * ( y + 0.5 ) / canvas.height;

  // s = e + uu + vv - dv
  glMatrix.vec3.scale(uu,this.sdr_u,u);
  glMatrix.vec3.scale(vv,this.sdr_v,v);
  glMatrix.vec3.add(vec3temp,this.eye,uu);
  glMatrix.vec3.add(vec3temp,vec3temp,vv);
  // la distanza focale è 1 ed è data da w (che è normalizzato)
  glMatrix.vec3.subtract(vec3temp,vec3temp,this.sdr_w);
  // p(t) = e + t(s-e) ---> p + dir
  glMatrix.vec3.subtract(vec3temp,vec3temp,this.eye);
  // normalize
  glMatrix.vec3.normalize(vec3temp,vec3temp);
  if (DEBUG) {
    console.log("*** cast ray ***");
    console.log("u: "+u);
    console.log("v: "+v);
    console.log("p: "+this.eye);
    console.log("d: "+vec3temp);
    console.log("****************");
  }
  return new Ray(this.eye,vec3temp);
}

/**
 * Create a new Ray composed by origin and direction
 *
 * @param {vec3} p origin point
 * @param {vec3} d direction vector
*/
var Ray = function(p,d){
  this.p = p;
  this.d = d;
  if (DEBUG) {
    console.log("*** new ray ***");
    console.log("p: "+this.p);
    console.log("d: "+this.d);
    console.log("****************");
  }
}

/**
 * Point observed from origin by ray
 *
 * @param {Number} t solution for intersection
 * @returns {vec3} vec3temp point observed from p to t * d
*/
Ray.prototype.point_at = function(t){
  var vec3temp = glMatrix.vec3.create();
  glMatrix.vec3.scale(vec3temp,this.d,t);
  glMatrix.vec3.add(vec3temp,vec3temp,this.p);
  if (DEBUG) {
    // p e d sono già trasformati.
    console.log("*** point at ***");
    console.log("p: "+vec3temp);
    console.log("****************");
  }
  return vec3temp;
}

/**
 * Transform ray (point an direction) using a transformation matrix
 *
 * @param {mat4} a s
 * @returns {Ray} a new ray trasformed
*/
Ray.prototype.transform = function(mat4){
  var p = glMatrix.vec4.create();
  var d = glMatrix.vec4.create();
  // p è un punto
  p[0] = this.p[0];
  p[1] = this.p[1];
  p[2] = this.p[2];
  p[3] = 1;
  // dir è un vettore
  d[0] = this.d[0];
  d[1] = this.d[1];
  d[2] = this.d[2];
  d[3] = 0;
  // trasformo
  glMatrix.vec4.transformMat4(p,p,mat4);
  glMatrix.vec4.transformMat4(d,d,mat4);
  // restituisco un nuovo raggio
  return new Ray([p[0],p[1],p[2]],[d[0],d[1],d[2]]);
}

/**
 * Lights content store
 *
 * @param {json}
*/
var Lights = function(json){
  this.ambient = { color : [0.0,0.0,0.0]};
  this.point = [];
  this.directional = [];

  for (var i = 0; i < json.length; i++) {
    if (json[i].source === 'Ambient') {
      this.ambient = new AmbientLight(json[i]);
    }
    if (json[i].source === 'Point') {
      // could be more than one
      this.point.push(new PointLight(json[i]));

    }
    if (json[i].source === 'Directional') {
      // could be more than one
      this.directional.push(new DirectionalLight(json[i]));
    }
  }
};

/**
 * Ambient Light
 *
 * @param {json}
*/
var AmbientLight = function(json){
  this.color = json.color;
}

/**
 * Point Light
 *
 * @param {json}
*/
var PointLight = function(json){
  this.color = json.color;
  this.position = json.position;
}
/**
 * Directional Light
 *
 * @param {json}
*/
var DirectionalLight = function(json){
  this.color = json.color;
  this.direction = json.direction;
}

/**
 * Material content store
 *
 * @param {json}
*/
var Material = function(json){
  this.ka = json.ka;
  this.kd = json.kd;
  this.ks = json.ks;
  this.shininess = json.shininess;
  this.kr = json.kr;
};

/**
 * Surface init
 *
 * @param {string} name of Surface
 * @param {Material} material of surface
 * @param {json} transformations for surface
*/
var Surface = function(name,material,transformations){
  this.name = name;
  this.material = material;
  this.transformations = transformations;
  this.transformMatrix = new TransformationMatrix();
  //this.transformMatrixInverted = new TransformationMatrix();

  //loading transformations
  for (var i = 0; i < transformations.length; i++) {
    if (transformations[i][0] === 'Translate'){
      this.transformMatrix.translate(transformations[i][1]);
    }
    if (transformations[i][0] === 'Rotate'){
      this.transformMatrix.rotate(transformations[i][1]);
    }
    if (transformations[i][0] === 'Scale'){
      this.transformMatrix.scale(transformations[i][1]);
    }
  }
  // invert transformMatrix to transoform ray instead surface
  this.transformMatrixInverted = this.transformMatrix.invert();
}

/**
* Check if a point is in shadow
*
* @param {vec3} point  to check if is in shadow
* @param {vec3} lightdir direction
* @param {Number} l_dist_max distance from light
* @param {Array[Surfaces]} surfaces on scene
* @returns {Boolean} True if point is in shadow
*/
Surface.prototype.shadow = function(point,lightdir,l_dist_max,surfaces){
  var s_ray = {};
  var t = {};
  var ray = new Ray(point,lightdir);
  // per ogni superficie
  for (var i = 0; i < surfaces.length; i++) {
    [s_ray,t] = surfaces[i].intersects(ray,EPSILON,Number.POSITIVE_INFINITY);
    // s_ray è un versore. la distanza è data da t
    if ((t>0) && (t< l_dist_max)){
      return true;
    }
  }
  return false;
};

/**
* Phong shading algorithm
*
* @param {vec3} cam position
* @param {vec3} point position
* @param {vec3} normal
* @param {Array[Lights]} lights on scene
* @param {Array[Surfaces]} surfaces on scene
* @param {Number} depth bounce
*
* @returns {vec3} pixel color
*/
Surface.prototype.shade = function(cam,point,normal,lights,surfaces,depth){
  var r = glMatrix.vec3.create();
  var ambient = glMatrix.vec3.create();
  var diffuse = glMatrix.vec3.create();
  var specular = glMatrix.vec3.create();
  var reflected = glMatrix.vec3.create();
  var color= glMatrix.vec3.create();
  var v = glMatrix.vec3.create();
  var l = glMatrix.vec3.create();
  var ldotn = {};
  var i ={};
  var l_dist_max = Number.POSITIVE_INFINITY;
  var new_view_ray = {};

  // Normal sphere testing code
  // if (this.name==="Sphere") {
  //   return [Math.abs(normal[0]),Math.abs(normal[1]),0];
  // }

  if (depth < 0) {
    return color;
  }

  // compute view direction versor
  glMatrix.vec3.subtract(v,cam,point);
  glMatrix.vec3.normalize(v,v);

  // ambient component computation
  glMatrix.vec3.multiply(ambient,this.material.ka,lights.ambient.color);
  glMatrix.vec3.add(color,color,ambient);

  // point lights
  for (var k = 0; k < lights.point.length; k++) {
    // compute light direction  versor
    glMatrix.vec3.subtract(l,lights.point[k].position,point);
    l_dist_max = glMatrix.vec3.length(l);
    glMatrix.vec3.normalize(l,l);
    // shadow computation
    if (this.shadow(point,l,l_dist_max,surfaces)){
       // if pixel in shadow, skip specular and diffuse radiation
       continue;
    }

    // specular component computation
    ldotn = glMatrix.vec3.dot(l,normal);
    glMatrix.vec3.scale(r,normal,2*ldotn);
    glMatrix.vec3.subtract(r,r,l);
    i = Math.pow(Math.max(0,glMatrix.vec3.dot(r,v)),this.material.shininess);
    glMatrix.vec3.scale(specular,this.material.ks,i);
    glMatrix.vec3.multiply(specular,specular,lights.point[k].color);
    glMatrix.vec3.add(color,color,specular);

    // diffuse component computation
    i = Math.max(0,glMatrix.vec3.dot(l,normal));
    glMatrix.vec3.scale(diffuse,this.material.kd,i);
    glMatrix.vec3.multiply(diffuse,diffuse,lights.point[k].color);
    glMatrix.vec3.add(color,color,diffuse);
  }

  //directional Lights
  for (var k = 0; k < lights.directional.length; k++) {
    // compute light direction  versor
    glMatrix.vec3.normalize(l,lights.directional[k].direction);
    glMatrix.vec3.negate(l,l);
    //***********************************************************************
    // shadow computation
     if (this.shadow(point,l,Number.POSITIVE_INFINITY,surfaces)){
       // if pixel in shadow, skip specular and diffuse radiation
       continue;
    }
    // specular component computation
    ldotn = 2 * glMatrix.vec3.dot(l,normal);
    glMatrix.vec3.scale(r,normal,ldotn);
    glMatrix.vec3.subtract(r,r,l);
    i = Math.pow(Math.max(0,glMatrix.vec3.dot(r,v)),this.material.shininess);
    glMatrix.vec3.scale(specular,this.material.ks,i);
    glMatrix.vec3.multiply(specular,specular,lights.directional[k].color);
    glMatrix.vec3.add(color,color,specular);

    // diffuse component computation
    i = Math.max(0,glMatrix.vec3.dot(l,normal));
    glMatrix.vec3.scale(diffuse,this.material.kd,i);
    glMatrix.vec3.multiply(diffuse,diffuse,lights.directional[k].color);
    glMatrix.vec3.add(color,color,diffuse);
  }

  // se una superficie non è riflettente mi risparmio di calcolare questa componentes
  if (this.material.kr[0] !== 0 || this.material.kr[1] !== 0 || this.material.kr[1] !== 0){
    // reflection light
    // calcolo la nuova direzione di vista
    glMatrix.vec3.negate(v,v);
    ldotn = glMatrix.vec3.dot(v,normal);
    glMatrix.vec3.scale(r,normal,2*ldotn);
    glMatrix.vec3.subtract(r,v,r);
    new_view_ray = new Ray(point,r);
    // find surface intersection
    var tbest = Number.POSITIVE_INFINITY;
    var firstsurfaceindex = null;
    for (var k = 0; k < surfaces.length; k++) {
      [s_ray, t]= surfaces[k].intersects(new_view_ray,0,tbest);
      if (t>0) {
        // hit in obj coordinates
        firstsurfaceindex = k;
        tbest = t;
        raybest = s_ray;
      }
    }
    if(firstsurfaceindex != null){
      var p = raybest.point_at(tbest);
      var trans_p = glMatrix.vec4.create();
      trans_p[0] = p[0];
      trans_p[1] = p[1];
      trans_p[2] = p[2];
      trans_p[3] = 1;
      glMatrix.vec4.transformMat4(trans_p,trans_p,surfaces[firstsurfaceindex].transformMatrix.mat);
      var n = surfaces[firstsurfaceindex].getNormal(p);
      reflected = surfaces[firstsurfaceindex].shade(point,trans_p,n,lights,surfaces,--depth);
      glMatrix.vec3.multiply(reflected,this.material.kr,reflected);
    };
    glMatrix.vec3.add(color,color,reflected);
  }
  if (DEBUG) {
    console.log("*** shading ***");
    console.log("cam:   "+cam);
    console.log("normal:"+normal);
    console.log("l:     "+l);
    console.log("r:"+r);
    console.log("v:"+v);
    console.log("\nambient:  "+ambient);
    console.log("specular: "+specular);
    console.log("diffuse:  "+diffuse);
    console.log("****************");
  }
  //return [1.0,1.0,1.0];
  return color;
}


/**
 * Sphere init
 *
 * @param {vec3} center of sphere
 * @param {vec3} radius of sphere
 * @param {Material} material of sphere
 * @param {json} transformation commands
*/
var Sphere= function(center, radius, material,transformation){
  Surface.call(this,'Sphere',material,transformation);
  this.center = center
  this.radius = radius;
};

// subclass extends superclass
Sphere.prototype = Object.create(Surface.prototype);
Sphere.prototype.constructor = Sphere;

/**
 * Ray-Sphere intersection
 *
 * @param {Ray} ray defined by p and dir
 * @param {Number} tmin min value for solution accepted
 * @param {Number} tmax max value for solution accepted
 * @returns {[Ray,Number]} new ray (translated) and t solution value, -1 if not intersects
*/
Sphere.prototype.intersects = function(ray,tmin,tmax){
  var a,b,c,d,t;
  var oc = glMatrix.vec3.create();
  var s_ray = ray;
  if (this.transformations.length > 0 ){
    // new trasformed ray, if surface is trasformed
     s_ray = ray.transform(this.transformMatrixInverted.mat);
  }
  glMatrix.vec3.subtract(oc, s_ray.p,this.center);
  a = glMatrix.vec3.dot( s_ray.d, s_ray.d);
  b = 2* glMatrix.vec3.dot(oc, s_ray.d);
  c = glMatrix.vec3.dot(oc,oc)- this.radius * this.radius;
  d = b*b-4*a*c;
  if ( d<0 ){
    return [ s_ray,-1];
  }
  t = (-b -Math.sqrt(d)) / (2.0 *a);
  if (t < tmin || t > tmax) {
    return [ s_ray,-1];
  }
  return [ s_ray,t];
};

/**
 * Normal computing for point on sphere
 *
 * @param {vec3} point on surface
 * @returns {vec3} normal at point on surface
*/
Sphere.prototype.getNormal = function(point){
  var vec3temp = glMatrix.vec3.create();
  var mat4temp = glMatrix.mat4.create();
  var p = glMatrix.vec4.create();
  glMatrix.vec3.subtract(p,point,this.center)
  p[3] = 0;
  glMatrix.mat4.transpose(mat4temp,this.transformMatrixInverted.mat);
  glMatrix.vec4.transformMat4(p,p,mat4temp);
  return glMatrix.vec3.normalize(p,p);
}

/**
 * Triangle init
 *
 * @param {Number} p1 vertice of triangle
 * @param {Number} p2 vertice of triangle
 * @param {Number} p3 vertice of triangle
 * @param {Material} material of triangle
 * @param {json} transformation commands
*/
var Triangle= function(p1,p2,p3,material,transformation){
  Surface.call(this,'Triangle',material,transformation);
  this.p1 = p1;
  this.p2 = p2;
  this.p3 = p3;
  // computing surface normal
  var ab = glMatrix.vec3.create();
  var bc = glMatrix.vec3.create();
  this.n = glMatrix.vec3.create();
  glMatrix.vec3.subtract(ab,this.p2,this.p1);
  glMatrix.vec3.subtract(bc,this.p3,this.p2);
  console.log(ab);
  console.log(bc);
  glMatrix.vec3.cross(this.n,ab,bc);
  glMatrix.vec3.normalize(this.n,this.n);
  var p = glMatrix.vec4.create();
  var mat4temp = glMatrix.mat4.create();
  p[0] = this.n[0];
  p[1] = this.n[1];
  p[2] = this.n[2];
  p[3] = 0;
  glMatrix.mat4.transpose(mat4temp,this.transformMatrixInverted.mat);
  glMatrix.vec4.transformMat4(p,p,mat4temp);
  this.n[0]=p[0];
  this.n[1]=p[1];
  this.n[2]=p[2];
};

// subclass extends superclass
Triangle.prototype = Object.create(Surface.prototype);
Triangle.prototype.constructor = Triangle;

/**
 * Ray-Triangle intersection
 * Resolve the system with Cramer's rule
 * as described on pag 10 of rt_chap4.pdf
 *
 * @param {json} ray defined by p and dir
 * @param {Number} tmin min value for solution accepted
 * @param {Number} tmax max value for solution accepted
 * @returns {[Ray,Number]} new ray (translated) and t solution value, -1 if not intersects
*/
Triangle.prototype.intersects= function(ray,tmin,tmax){
  var mat4temp = glMatrix.mat4.create();
  var s_ray = ray;
  if (this.transformations.length > 0 ){
    // new trasformed ray, if surface is trasformed
    s_ray = ray.transform(this.transformMatrixInverted.mat);
  }
  var a = this.p1[0]-this.p2[0];
  var b = this.p1[1]-this.p2[1];
  var c = this.p1[2]-this.p2[2];
  var d = this.p1[0]-this.p3[0];
  var e = this.p1[1]-this.p3[1];
  var f = this.p1[2]-this.p3[2];
  var g =  s_ray.d[0];
  var h =  s_ray.d[1];
  var i =  s_ray.d[2];
  var j = this.p1[0]- s_ray.p[0];
  var k = this.p1[1]- s_ray.p[1];
  var l = this.p1[2]- s_ray.p[2];
  var m = a * ( e*i - h*f ) + b * ( g*f - d*i ) + c * ( d*h - e*g);
  // t valid from 0 to +Inf
  var t =   - ( f * ( a*k - j*b ) + e * ( j*c - a*l ) + d * ( b*l - k*c )) /m
  if (t < 0 || t < tmin || t >= tmax){
    return [ s_ray,-1];
  }
  // gamma valid from 0 to 1
  var gamma = ( i * ( a*k - j*b ) + h * ( j*c - a*l ) + g * ( b*l - k*c )) /m
  if ((gamma < 0) || (gamma > 1)){
    return [ s_ray,-1];
  }
  var beta =  ( j * ( e*i - h*f ) + k * ( g*f - d*i ) + l * ( d*h - e*g )) / m ;
  // beta valid from 0 to 1, beta + gamma must be <= 1
  if ((beta < 0) || (beta > 1 - gamma)){
    return [ s_ray,-1];
  }
  return [ s_ray,t];
};

/**
 * Normal computing for point on triangle
 *
 * @param {vec3} point on surface (passed but not used)
 * @returns {vec3} normal at point on triangle
*/
Triangle.prototype.getNormal = function(point){
  return this.n;
}

/**
 * Transformation Matrix init: create a identity matrix
 *
*/
var TransformationMatrix = function(){
  this.mat = glMatrix.mat4.create();
}

/**
 * Transformation Matrix: add translation
 *
 * @param {vec3} vec with translation value
*/
TransformationMatrix.prototype.translate = function(vec){
  var tmp = glMatrix.mat4.create();
  glMatrix.mat4.translate(tmp,tmp,vec);
  glMatrix.mat4.multiply(this.mat,this.mat,tmp);
}

/**
 * Transformation Matrix: add rotation
 *
 * @param {vec3} vec with rotation value
*/
TransformationMatrix.prototype.rotate = function(vec){
  var tmp = glMatrix.mat4.create();
  glMatrix.mat4.rotate(tmp, tmp, vec[0]*Math.PI/180, [1,0,0]);
  glMatrix.mat4.rotate(tmp, tmp, vec[1]*Math.PI/180, [0,1,0]);
  glMatrix.mat4.rotate(tmp, tmp, vec[2]*Math.PI/180, [0,0,1]);
  glMatrix.mat4.multiply(this.mat,this.mat,tmp);
}

/**
 * Transformation Matrix: add scale
 *
 * @param {vec3} vec with scale value
*/
TransformationMatrix.prototype.scale = function(vec){
  var tmp = glMatrix.mat4.create();
  glMatrix.mat4.scale(tmp,tmp,vec);
  glMatrix.mat4.multiply(this.mat,this.mat,tmp);
}

/**
 * Transformation Matrix: invert matrix and create a new one
 *
* @returns {TransformationMatrix} inverted matrix
*/
TransformationMatrix.prototype.invert = function(){
  var tmp = new TransformationMatrix();
  glMatrix.mat4.invert(tmp.mat,this.mat);
  return tmp;
}
