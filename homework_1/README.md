## Computer graphics

Author      : Moris Pozzati  
Mat number  : 073344  
Description : homework 1  
Date        : 2018/10/26  

### Exercise 1: Implementation
Per disegnare i rettangoli in modo dinamico si consideri la variabile globale **nr** *(New rectangle)*:  
è una variabile booleana che descrive lo stato in cui ci si trova sul canvas:  
- *false* rappresenta lo stato in cui non è attesa nessuna modifica del buffer degli elementi (non si sta disegnando)  
- *true* rappresenta lo stato in cui si sta modificando il vettore da inserire nel buffer degl elementi  
Lo stato iniziale di nr è *false*.  
L'**onclick** è l'evento che triggera la variabile *nr*.  
Sull'onclick di attivazione vengolo inseriti quattro nuovi vertici nell'array, tutti situati nello stesso punto (x,y) con il colore presente nel picker in quel momento.
Dal momento in cui nr = true l'evento **onmove** modifica la posizione (x,y) dei tre vertici non ancora definiti ma già presenti nell'array.  
Un nuovo evento **onclick** interrompe la modifica e riporta nr a false, definendo cosi le dimensioni finali del rettangolo disegnato.  


Alla pressione della combinazione **CTRL+Z** l'array viene svuotato degli ultimi quattro elementi inseriti. inoltre viene reimpostata a false la variabile nr

### Exercise 2
In generale è stato mantenuto lo stesso template fornito per l'esercitazione.  
Le funzioni utilizzate per la gestione della grafica sono le seguenti:  
- **initArrayBuffer**: crea gli oggetti di tipo buffer  
- **bufferize**: scrive gli array con le proprietà nei buffers  
- **inscribedPolygon**: la costruzione di tutti i poligoni è demandata a questa funzione che, dato un raggio *r*, numero di punti *n* e punto di partenza *s* costruisce un poligono inscritto di *n* lati nella circonferenza di raggio *r* con un vertice in *s*.  
- le funzioni **initVertexBuffers...** si limitano a richiamare la *inscribedPolygon* e, dove necessario, effettuare alcuni calcoli per ottenere il raggio della circonferenza per il poligono delle dimensioni richieste. Anche il triangolo viene disegnato in questo modo. Verrano quindi impiegati tre triangoli isosceli con un vertice in comune per disegnare il triangolo equilatero. La scelta può essere discutibile ma vista la scarsa richiesta di risorse richieste dalla macchina in questa esercitazioneho  deciso di mantenere questa implementazione. Ad ogni modo è presente la parte di codice che utlizza una matrice statica per disegnare i vertici. Ragionamento analogo può essere fatto per il quadrato, realizzato utilizzando quattro triangoli anzichè due.  
