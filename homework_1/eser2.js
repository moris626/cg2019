/******************************************************************************
Author      : Moris Pozzati
Mat number  : 073344
Description : homework 1
Date        : 2018/10/26
*******************************************************************************/

// eser2.js
// Vertex shader program
var VSHADER_SOURCE =
  'attribute vec4 a_Position;\n'      + // Vertex coordinates
  'attribute vec4 a_Color;\n'  +        // Vertex Color
  'uniform mat4 u_MvpMatrix;\n'       + // Model-View-Projection Matrix
  'varying vec4 v_Color;\n'           + // vertex color
  'void main() {\n'                   +
  '  gl_Position = u_MvpMatrix * a_Position;\n' +
  '  v_Color = a_Color;\n' +
  '}\n';
//******************************************************************************
// Fragment shader program
var FSHADER_SOURCE =
  '#ifdef GL_ES\n' +
  'precision mediump float;\n' +
  '#endif\n' +
  'varying vec4 v_Color;\n' +
  'void main() {\n' +
  '  gl_FragColor = v_Color;\n' +
  '}\n';
//******************************************************************************
function main() {
  // creo una GUI con dat.gui
  var gui = new dat.GUI();
  // checkbox geometry
  var geometria = {
    triangle:true,
    square:false,
    circle:false,
    pentagon:false,
    hexagon:false
  };

  // color selector
  var colore = {color:[255,0,0]};
  // Retrieve <canvas> element
  var canvas = document.getElementById('webgl');
  // Get the rendering context for WebGL
  var gl = getWebGLContext(canvas);
  if (!gl) {
    console.log('Failed to get the rendering context for WebGL');
    return;
  }
  // Initialize shaders
  if (!initShaders(gl, VSHADER_SOURCE, FSHADER_SOURCE)) {
    console.log('Failed to intialize shaders.');
    return;
  }
  // Set the clear color and enable the depth test
  gl.clearColor(0, 0, 0, 1);
  gl.enable(gl.DEPTH_TEST);
  // Get the storage locations of uniform variables and so on
  var u_MvpMatrix      = gl.getUniformLocation(gl.program, 'u_MvpMatrix');
  if (!u_MvpMatrix ) {
    console.log('Failed to get the storage location');
    return;
  }
  var vpMatrix = new Matrix4();   // View projection matrix
  var camPos = new Vector3([0.0,0.0,10.0]);
  // Calculate the view projection matrix
  vpMatrix.setPerspective(30, canvas.width/canvas.height, 1, 1000);
  vpMatrix.lookAt(camPos.elements[0],camPos.elements[1],camPos.elements[2], 0, 0, 0, 0, 1, 0);
  var modelMatrix = new Matrix4();  // Model matrix
  var mvpMatrix = new Matrix4();    // Model view projection matrix
  var n = initVertexBuffersTriangle(gl,colore,2);
  if (n < 0) {
    console.log('Failed to set the vertex information');
    return;
  }
  //***************************************************************************
  // dat.GUI managing
  gui.addColor(colore,'color').onFinishChange(function(value) {
    for (var i in gui.__controllers) {
      gui.__controllers[i].updateDisplay();
    }
    if(geometria.triangle) n = initVertexBuffersTriangle(gl,colore,2);
    if(geometria.square) n = initVertexBuffersSquare(gl,colore,2);
    if(geometria.circle) n = initVertexBuffersCircle(gl,colore,1);
    if(geometria.pentagon) n = initVertexBuffersPentagon(gl,colore,1);
    if(geometria.hexagon) n = initVertexBuffersHexagon(gl,colore,1);
  });
  gui.add(geometria,'triangle').onFinishChange(function(value) {
    // Fires when a controller loses focus.
	  if(value == true){
  		geometria.triangle = value;
  		geometria.square = false;
  		geometria.circle = false;
  		geometria.pentagon = false;
  		geometria.hexagon = false;
  		n = initVertexBuffersTriangle(gl,colore,2);
    }
    // Iterate over all controllers
    for (var i in gui.__controllers) {
      gui.__controllers[i].updateDisplay();
    }
  });
  gui.add(geometria,'square').onFinishChange(function(value) {
    // Fires when a controller loses focus.
    if(value == true){
  		geometria.triangle = false;
  		geometria.square   = value;
  		geometria.circle   = false;
  		geometria.pentagon = false;
  		geometria.hexagon  = false;
  		n = initVertexBuffersSquare(gl,colore,2);
	  }
    // Iterate over all controllers
    for (var i in gui.__controllers) {
      gui.__controllers[i].updateDisplay();
    }
  });
  gui.add(geometria,'circle').onFinishChange(function(value) {
    // Fires when a controller loses focus.
    if(value == true){
      geometria.traingle = false;
      geometria.square   = false;
      geometria.circle   = value;
      geometria.pentagon = false;
      geometria.hexagon  = false;
      n = initVertexBuffersCircle(gl,colore,1);
    }
    // Iterate over all controllers
    for (var i in gui.__controllers) {
      gui.__controllers[i].updateDisplay();
    }
  });
  gui.add(geometria,'pentagon').onFinishChange(function(value) {
    // Fires when a controller loses focus.
    if(value == true){
      geometria.traingle = false;
      geometria.square   = false;
      geometria.circle   = false;
      geometria.pentagon = value;
      geometria.hexagon  = false;
      n = initVertexBuffersPentagon(gl,colore,1);
    }
    // Iterate over all controllers
    for (var i in gui.__controllers) {
      gui.__controllers[i].updateDisplay();
    }
  });
  gui.add(geometria,'hexagon').onFinishChange(function(value) {
    // Fires when a controller loses focus.
    if(value == true){
      geometria.traingle = false;
      geometria.square   = false;
      geometria.circle   = false;
      geometria.pentagon = false;
      geometria.hexagon  = value;
      n = initVertexBuffersHexagon(gl,colore,1);
    }
    // Iterate over all controllers
    for (var i in gui.__controllers) {
      gui.__controllers[i].updateDisplay();
    }
  });
  //****************************************************************************
  var tick = function() {
    mvpMatrix.set(vpMatrix)
    gl.uniformMatrix4fv(u_MvpMatrix, false, mvpMatrix.elements);
    // Clear color and depth buffer
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    // Draw the cube
    gl.drawElements(gl.TRIANGLES, n, gl.UNSIGNED_SHORT, 0);
    requestAnimationFrame(tick, canvas); // Request that the browser ?calls tick
  };
  tick();
}

/************************************************************/
/* initVertexBuffersTriangle(gl): create triangle           */
/************************************************************/
function initVertexBuffersTriangle(gl,color,side_len) {
  var radius =  side_len*Math.sqrt(3)/3;
  return inscribedPolygon(gl,color,radius,Math.PI/2,3);
  // fixed array method
  // // Coordinates
  // var vertices = new Float32Array([
	//  0.0,2.0*Math.sqrt(3.0)/3.0,0.0, //v0
	// -1.0,   -Math.sqrt(3.0)/3.0,0.0, //v1
	//  1.0,   -Math.sqrt(3.0)/3.0,0.0  //v2
  // ]);
  //
  // // Colors
  // var colors = new Float32Array([
  //   1.0,0.0,0.0,
	//   1.0,0.0,0.0,
  //   1.0,0.0,0.0
  // ]);
  //
  // // Indices of the vertices
  // var indices = new Uint16Array([
	// 0,1,2
  // ]);
  //
  // bufferize(gl,vertices,colors,indices);
  //
  // return indices.length;
}
/************************************************************/
/* initVertexBuffersSquare(gl): create square               */
/************************************************************/
function initVertexBuffersSquare(gl,color,side_len) {
  var radius =  Math.sqrt(Math.pow(side_len,2)/2);
  return inscribedPolygon(gl,color,radius,Math.PI/4,4);
  // fixed array method
  // // Coordinates
  // var vertices = new Float32Array([
  //  -1.0,-1.0,0.0, //v0
  //  -1.0, 1.0,0.0, //v1
  //   1.0, 1.0,0.0, //v2
  //   1.0,-1.0,0.0, //v3
  // ]);
  //
  // // Colors
  // var colors = new Float32Array([
  //   1.0,0.0,0.0,
  //   1.0,0.0,0.0,
  //   1.0,0.0,0.0,
  //   1.0,0.0,0.0
  // ]);
  //
  // // Indices of the vertices
  // var indices = new Uint16Array([
  // 0,1,2,
  // 0,2,3
  // ]);
  //
  // bufferize(gl,vertices,colors,indices);
  // return indices.length;
}
/************************************************************/
/* initVertexBuffersCircle(gl): create circle               */
/************************************************************/
function initVertexBuffersCircle(gl,color,radius) {
  return inscribedPolygon(gl,color,radius,0,360);
}
/************************************************************/
/* initVertexBuffersPentagon(gl): create pentagon           */
/************************************************************/
function initVertexBuffersPentagon(gl,color,side_len) {
  var radius = 2 * side_len / Math.sqrt(10-2*Math.sqrt(5));
  return inscribedPolygon(gl,color,radius,Math.PI/2,5);
}
/************************************************************/
/* initVertexBuffersHexagon(gl): create hexagon             */
/************************************************************/
function initVertexBuffersHexagon(gl,color,side_len) {
  return inscribedPolygon(gl,color,side_len,0,6);
}
/************************************************************
*  inscribedPolygon(gl): create a generic regular polygon    *
*  with n_points vertices inscribed in a circle where radius *
*  defined                                                   *
*************************************************************/
function inscribedPolygon(gl,color,radius,start,n_points){
  var step = 2*Math.PI / n_points;
  var i = 0;
  // Add first vertice, the center of circle
  var vertices = [0.0,0.0,0.0];
  var indices = [];
  var colors = [];
  // add color for 1st vertice
  colors.push(color.color[0]/255,color.color[1]/255,color.color[2]/255);
  //colors.push(0,0,0);
  for(i = start; i<start+Math.PI*2;i+=step){
    //Use sin and cos to extraxt (x,y)
    vertices.push(radius*Math.cos(i),radius*Math.sin(i),0.0);
    colors.push(color.color[0]/255,color.color[1]/255,color.color[2]/255);
  }
  //create indices array
  for(i=1;i<=vertices.length/3-2;i++){
    indices.push(0,i,i+1);
  }
  //creating last triangles V0 , Vn , V1
  indices.push(0,i,1)
  var verticesArray = new Float32Array(vertices);
  var indicesArray = new Uint16Array(indices);
  var colorsArray = new Float32Array(colors);
  // put arrays in gl buffer
  bufferize(gl,verticesArray,colorsArray,indicesArray);
  return indices.length;
}

//*****************************************************************************
/************************************************************
* BUFFER utility functions                                  *
************************************************************/
function initArrayBuffer(gl, attribute, data, num, type) {
  // Create a buffer object
  var buffer = gl.createBuffer();
  if (!buffer) {
    console.log('Failed to create the buffer object');
    return false;
  }
  // Write date into the buffer object
  gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
  gl.bufferData(gl.ARRAY_BUFFER, data, gl.STATIC_DRAW);
  // Assign the buffer object to the attribute variable
  var a_attribute = gl.getAttribLocation(gl.program, attribute);
  if (a_attribute < 0) {
    console.log('Failed to get the storage location of ' + attribute);
    return false;
  }
  gl.vertexAttribPointer(a_attribute, num, type, false, 0, 0);
  // Enable the assignment of the buffer object to the attribute variable
  gl.enableVertexAttribArray(a_attribute);
  return true;
}

function bufferize(gl,vertices,colors,indices){
  // Write the vertex property to buffers (coordinates, colors and normals)
  if (!initArrayBuffer(gl, 'a_Position', vertices, 3, gl.FLOAT)) return -1;
  if (!initArrayBuffer(gl, 'a_Color'   , colors  , 3, gl.FLOAT)) return -1;
  // Unbind the buffer object
  gl.bindBuffer(gl.ARRAY_BUFFER, null);
  // Write the indices to the buffer object
  var indexBuffer = gl.createBuffer();
  if (!indexBuffer) {
    console.log('Failed to create the buffer object');
    return false;
  }
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, indices, gl.STATIC_DRAW);
}
