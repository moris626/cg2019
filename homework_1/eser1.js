/******************************************************************************
Author      : Moris Pozzati
Mat number  : 073344
Description : homework 1
Date        : 2018/10/26
*******************************************************************************/

// Vertex shader program
var VSHADER_SOURCE =
  'attribute vec4 a_Position;\n' +
  'attribute vec4 a_Color;\n' +
  'varying vec4 v_Color;\n' +
  'void main() {\n' +
  '  gl_Position = a_Position;\n' +
  '  v_Color = a_Color;\n' +
  '}\n';

// Fragment shader program
var FSHADER_SOURCE =
  '#ifdef GL_ES\n' +
  'precision mediump float;\n' +
  '#endif\n' +
  'varying vec4 v_Color;\n' +
  'void main() {\n' +
  '  gl_FragColor = v_Color;\n' +
  '}\n';


// Global var
// vertices  and colors
var vc = [];
// Indices of the vertices
var i = [];
var colore = {color0:[255,255,255,255]};
// state of draw
// true : drawing
// false : not drawing
var nr = false;

// main function loaded on body
function main() {

  // creo una GUI con dat.gui
  var gui = new dat.GUI();
  gui.addColor(colore,'color0').onFinishChange(function(value) {
  });
  // Retrieve <canvas> element
  var canvas = document.getElementById('webgl');

  // Get the rendering context for WebGL
  var gl = getWebGLContext(canvas);
  if (!gl) {
    console.log('Failed to get the rendering context for WebGL');
    return;
  }

  // Initialize shaders
  if (!initShaders(gl, VSHADER_SOURCE, FSHADER_SOURCE)) {
    console.log('Failed to intialize shaders.');
    return;
  }

  canvas.onmousedown = function(ev) {click(ev,canvas);};
  canvas.onmousemove = function(ev) {move(ev,canvas);};
  document.onkeydown = function(ev) {KeyPress(ev);};;


  // Set clear color
  gl.clearColor(0.0, 0.0, 0.0, 1.0);

  //funzione che attiva un flag che segnala al sistema se c'è necessita di ridisegnare la pagina
  //gli passo la funzione da richiamare e l'area da ridisegnare
  function render(){
    requestAnimationFrame(render,canvas);
    gl.clear(gl.COLOR_BUFFER_BIT);
    // Draw the rectangles
    draw(gl,vc,i);
  }
  render();
}

function draw(gl,vc,i) {
  // vertices and colors
  var verticesColors = new Float32Array(vc);
  // Indices of the vertices
  var indices = new Uint8Array(i);
  // Create a buffer object
  var vertexColorBuffer = gl.createBuffer();
  var indexBuffer = gl.createBuffer();
  if (!vertexColorBuffer || !indexBuffer) {
    return -1;
  }
  // Write the vertex coordinates and color to the buffer object
  gl.bindBuffer(gl.ARRAY_BUFFER, vertexColorBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, verticesColors, gl.STATIC_DRAW);
  var FSIZE = verticesColors.BYTES_PER_ELEMENT;
  // Assign the buffer object to a_Position and enable the assignment
  var a_Position = gl.getAttribLocation(gl.program, 'a_Position');
  if(a_Position < 0) {
    console.log('Failed to get the storage location of a_Position');
    return -1;
  }
  gl.vertexAttribPointer(a_Position, 3, gl.FLOAT, false, FSIZE * 6, 0);
  gl.enableVertexAttribArray(a_Position);
  // Assign the buffer object to a_Color and enable the assignment
  var a_Color = gl.getAttribLocation(gl.program, 'a_Color');
  if(a_Color < 0) {
    console.log('Failed to get the storage location of a_Color');
    return -1;
  }
  gl.vertexAttribPointer(a_Color, 3, gl.FLOAT, false, FSIZE * 6, FSIZE * 3);
  gl.enableVertexAttribArray(a_Color);
  // Write the indices to the buffer object
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, indices, gl.STATIC_DRAW);
  // draw rectangles as triangle couples
  gl.drawElements(gl.TRIANGLES, i.length, gl.UNSIGNED_BYTE, 0);
}

// at click event update the vc and i arrays
function click(ev,canvas) {
  var x1 = ev.clientX;
  var y1 = ev.clientY;
  var rect = ev.target.getBoundingClientRect();
  nr = ! nr;
  x1 = ((x1 - rect.left) - canvas.height / 2) / (canvas.height / 2);
  y1 = (canvas.width / 2 - (y1 - rect.top)) / (canvas.width / 2);
  if (nr){
    //save all vertices and color
    vc.push(x1,  y1,  0.5, colore.color0[0]/255,colore.color0[1]/255,colore.color0[2]/255);
    vc.push(x1,  y1,   0.5, colore.color0[0]/255,colore.color0[1]/255,colore.color0[2]/255);
    vc.push(x1,  y1,  0.5, colore.color0[0]/255,colore.color0[1]/255,colore.color0[2]/255);
    vc.push(x1,  y1,  0.5, colore.color0[0]/255,colore.color0[1]/255,colore.color0[2]/255);
    // add index vertices
    if( i.length === 0 ){
      i.push(0,1,2,0,1,3);
    }else {
      //console.log(i.length/6);
      i.push(4*i.length/6,
        4*i.length/6+1,
        4*i.length/6+2,
        4*i.length/6,
        4*i.length/6+1,
        4*i.length/6+3
      );
    }
  }
}

function move(ev,canvas){
  if(nr){
    //console.log("move");
    var x1 = ev.clientX;
    var y1 = ev.clientY;
    var rect = ev.target.getBoundingClientRect();
    x1 = ((x1 - rect.left) - canvas.height / 2) / (canvas.height / 2);
    y1 = (canvas.width / 2 - (y1 - rect.top)) / (canvas.width / 2);
    // 1   4
    // 3   2
    vc[vc.length-6] = x1;
    vc[vc.length-11] = y1;
    vc[vc.length-18] = x1;
    vc[vc.length-17] = y1;
  }

}

function KeyPress(ev) {
      var evtobj = window.event? event : e
      if (evtobj.keyCode == 90 && evtobj.ctrlKey) {
        //alert("Ctrl+z");
        nr = false;
        vc.splice(vc.length-24,24);
        i.splice(i.length-6,6);
      }
}
