/******************************************************************************
Author      : Moris Pozzati
Mat number  : 073344
Description : homework 2
Date        : 2018/11/26
*******************************************************************************/

// eser1.js
// Vertex shader program
var VSHADER_SOURCE =
  'attribute vec4 a_Position;\n'      + // Vertex coordinates
  'attribute vec4 a_Color;\n'  +        // Vertex Color
  'uniform mat4 u_MvpMatrix;\n'       + // Model-View-Projection Matrix
  'varying vec4 v_Color;\n'           + // vertex color
  'void main() {\n'                   +
  '  gl_Position = u_MvpMatrix * a_Position;\n' +
  '  v_Color = a_Color;\n' +
  '}\n';
//******************************************************************************************
// Fragment shader program
var FSHADER_SOURCE =
  '#ifdef GL_ES\n' +
  'precision mediump float;\n' +
  '#endif\n' +
  'varying vec4 v_Color;\n' +
  'void main() {\n' +
  '  gl_FragColor = v_Color;\n' +
  '}\n';
//*****************************************************************************************
function main() {
  // Retrieve <canvas> element
  var canvas = document.getElementById('webgl');
  // Get the rendering context for WebGL
  var gl = getWebGLContext(canvas);
  if (!gl) {
    console.log('Failed to get the rendering context for WebGL');
    return;
  }
  // Initialize shaders
  if (!initShaders(gl, VSHADER_SOURCE, FSHADER_SOURCE)) {
    console.log('Failed to intialize shaders.');
    return;
  }
  // Set the clear color and enable the depth test
  gl.clearColor(0, 0, 0, 1);
  gl.enable(gl.DEPTH_TEST);
  // Get the storage locations of uniform variables and so on
  var u_MvpMatrix      = gl.getUniformLocation(gl.program, 'u_MvpMatrix');
  if (!u_MvpMatrix ) {
    console.log('Failed to get the storage location');
    return;
  }
  var vpMatrix = new Matrix4();   // View projection matrix
  var camPos = new Vector3([0.0,-3.0,10.0]);
  // Calculate the view projection matrix
  vpMatrix.setPerspective(30, canvas.width/canvas.height, 1, 1000);
  vpMatrix.lookAt(camPos.elements[0],camPos.elements[1],camPos.elements[2], 0, 0, 0, 0, 1, 0);

  var currentAngle = 0.0;  // Current rotation angle
  var modelMatrix = new Matrix4();  // Model matrix
  var mvpMatrix = new Matrix4();    // Model view projection matrix
  //*********************************************************************
  // creo una GUI con dat.gui
  var gui = new dat.GUI();
  // checkbox geometry
  var geometria = {cube:true,cone:false,cylinder:false,sphere:false,torus:false};
  // color selector
  var colore = {color:[255,0,0]};
  var n = drawCube(gl,colore);
  if (n < 0) {
    console.log('Failed to set the vertex information');
    return;
  }
  //***************************************************************************
  // dat.GUI managing
  gui.addColor(colore,'color').onFinishChange(function(value) {
    for (var i in gui.__controllers) {
      gui.__controllers[i].updateDisplay();
    }
    if(geometria.cube) n = drawCube(gl,colore);
    if(geometria.cone) n = drawCone(gl,colore,1,2,14);
    if(geometria.cylinder) n = drawCylinder(gl,colore,1,2,14);
    if(geometria.sphere) n = drawSphere(gl,colore,1,12);
    if(geometria.torus) n = drawTorus(gl,colore,1,0.5,20,40);
  });
  gui.add(geometria,'cube').onFinishChange(function(value) {
    // Fires when a controller loses focus.
	  if(value == true){
  		geometria.cube = value;
  		geometria.cone = false;
  		geometria.cylinder = false;
  		geometria.sphere = false;
  		geometria.torus = false;
      n = drawCube(gl,colore);
  	}
    // Iterate over all controllers
    for (var i in gui.__controllers) {
      gui.__controllers[i].updateDisplay();
    }
  });
  gui.add(geometria,'cone').onFinishChange(function(value) {
    // Fires when a controller loses focus.
    if(value == true){
  		geometria.cube = false;
  		geometria.cone = value;
  		geometria.cylinder = false;
  		geometria.sphere = false;
  		geometria.torus = false;
      n = drawCone(gl,colore,1,2,14);
    }
    // Iterate over all controllers
    for (var i in gui.__controllers) {
      gui.__controllers[i].updateDisplay();
    }

  });
  gui.add(geometria,'cylinder').onFinishChange(function(value) {
    // Fires when a controller loses focus.
	  if(value == true){
  		geometria.cube = false;
  		geometria.cone = false;
  		geometria.cylinder = value;
  		geometria.sphere = false;
  		geometria.torus = false;
      n = drawCylinder(gl,colore,1,2,14);
	  }
	  // Iterate over all controllers
    for (var i in gui.__controllers) {
      gui.__controllers[i].updateDisplay();
    }
  });
  gui.add(geometria,'sphere').onFinishChange(function(value) {
    // Fires when a controller loses focus.
	  if(value == true){
  		geometria.cube = false;
  		geometria.cone = false;
  		geometria.cylinder = false;
  		geometria.sphere = value;
  		geometria.torus = false;
      n = drawSphere(gl,colore,1,12);
    }
	  // Iterate over all controllers
    for (var i in gui.__controllers) {
      gui.__controllers[i].updateDisplay();
    }
  });
  gui.add(geometria,'torus').onFinishChange(function(value) {
    // Fires when a controller loses focus.
	  if(value == true){
  		geometria.cube = false;
  		geometria.cone = false;
  		geometria.cylinder = false;
  		geometria.sphere = false;
  		geometria.torus = value;
      n = drawTorus(gl,colore,1,0.5,20,40);
    }
	  // Iterate over all controllers
    for (var i in gui.__controllers) {
      gui.__controllers[i].updateDisplay();
    }
  });
  //*********************************************************************************
  // Rotation angle (degrees/second)
  var ANGLE_STEP = 5.0;
  // Last time that this function was called
  var g_last = Date.now();
  function animate(angle) {
    // Calculate the elapsed time
    var now = Date.now();
    var elapsed = now - g_last;
    g_last = now;
    // Update the current rotation angle (adjusted by the elapsed time)
    var newAngle = angle + (ANGLE_STEP * elapsed) / 1000.0;
    return newAngle %= 360;
  }
  var tick = function() {
    currentAngle = animate(currentAngle);  // Update the rotation angle
    // Calculate the model matrix
    modelMatrix.setRotate(currentAngle, 0, 1, 0); // Rotate around the y-axis
    mvpMatrix.set(vpMatrix).multiply(modelMatrix);
    gl.uniformMatrix4fv(u_MvpMatrix, false, mvpMatrix.elements);
    // Clear color and depth buffer
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    // Draw the cube
    gl.drawElements(gl.TRIANGLES, n, gl.UNSIGNED_SHORT, 0);
    requestAnimationFrame(tick, canvas); // Request that the browser ?calls tick
  };
  tick();
}
//******************************************************************************
// Draw solid functions
function drawCube(gl,color) {
  // Create a cube
  //    v6----- v5
  //   /|      /|
  //  v1------v0|
  //  | |     | |
  //  | |v7---|-|v4
  //  |/      |/
  //  v2------v3
  // Coordinates
  var vertices = new Float32Array([
     1.0, 1.0, 1.0,  -1.0, 1.0, 1.0,  -1.0,-1.0, 1.0,   1.0,-1.0, 1.0, // v0-v1-v2-v3 front
     1.0, 1.0, 1.0,   1.0,-1.0, 1.0,   1.0,-1.0,-1.0,   1.0, 1.0,-1.0, // v0-v3-v4-v5 right
     1.0, 1.0, 1.0,   1.0, 1.0,-1.0,  -1.0, 1.0,-1.0,  -1.0, 1.0, 1.0, // v0-v5-v6-v1 up
    -1.0, 1.0, 1.0,  -1.0, 1.0,-1.0,  -1.0,-1.0,-1.0,  -1.0,-1.0, 1.0, // v1-v6-v7-v2 left
    -1.0,-1.0,-1.0,   1.0,-1.0,-1.0,   1.0,-1.0, 1.0,  -1.0,-1.0, 1.0, // v7-v4-v3-v2 down
     1.0,-1.0,-1.0,  -1.0,-1.0,-1.0,  -1.0, 1.0,-1.0,   1.0, 1.0,-1.0  // v4-v7-v6-v5 back
  ]);

  // Colors
  var i;
  var colors = [];
  for(i=0;i<32;i++){
    colors.push(color.color[0]/255,color.color[1]/255,color.color[2]/255);
  }
  var colorsArray = new Float32Array(colors);

  // Indices of the vertices
  var indices = new Uint16Array([
     0, 1, 2,   0, 2, 3,    // front
     4, 5, 6,   4, 6, 7,    // right
     8, 9,10,   8,10,11,    // up
    12,13,14,  12,14,15,    // left
    16,17,18,  16,18,19,    // down
    20,21,22,  20,22,23     // back
  ]);
  bufferize(gl,vertices,colorsArray,indices)
  return indices.length;
}

function drawCone(gl,color,radius,height,n_points){
  var step = 2*Math.PI / n_points;
  var i = 0;
  // Add first and second vertices, the center of bottom circle and the top of cone
  var vertices = [0.0,-height/2,0.0,0.0,height/2,0.0];
  var indices = [];
  var colors = [];
  // add color for 1st vertice
  colors.push(color.color[0]/255,color.color[1]/255,color.color[2]/255);
  colors.push(color.color[0]/255,color.color[1]/255,color.color[2]/255);
  //colors.push(0,0,0);
  for(i = 0; i<Math.PI*2;i+=step){
    //Use sin and cos to extraxt (x,y)
    vertices.push(radius*Math.cos(i),-height/2,radius*Math.sin(i));
    colors.push(color.color[0]/255,color.color[1]/255,color.color[2]/255);
  }
  //create indices array
  for(i=2;i<=vertices.length/3-2;i++){
    //bottom triangle
    indices.push(i,0,i+1);
    //side triangle
    indices.push(i,1,i+1);
  }
  //creating last triangles V0 , Vn , V1
  indices.push(i,0,2);
  indices.push(i,1,2);
  var verticesArray = new Float32Array(vertices);
  var indicesArray = new Uint16Array(indices);
  var colorsArray = new Float32Array(colors);
  // put arrays in gl buffer
  bufferize(gl,verticesArray,colorsArray,indicesArray);
  return indices.length;
}

function drawCylinder(gl,color,radius,height,n_points){
  var step = 2*Math.PI / n_points;
  var i = 0;
  // Add first and second vertices, the center of circle at the bottom and the circle at the top
  var vertices = [0.0,-height/2,0.0,0.0,height/2,0.0];
  var indices = [];
  var colors = [];
  // add color for 1st and 2nd vertices
  colors.push(color.color[0]/255,color.color[1]/255,color.color[2]/255);
  colors.push(color.color[0]/255,color.color[1]/255,color.color[2]/255);
  //colors.push(0,0,0);
  for(i = 0; i<Math.PI*2;i+=step){
    //Use sin and cos to extraxt (x,y). z is the height of cylinder
    vertices.push(radius*Math.cos(i),-height/2,radius*Math.sin(i));
    vertices.push(radius*Math.cos(i),height/2,radius*Math.sin(i));
    colors.push(color.color[0]/255,color.color[1]/255,color.color[2]/255);
    colors.push(color.color[0]/255,color.color[1]/255,color.color[2]/255);
  }
  //create indices array
  for(i=2;i<=vertices.length/3-3;i=i+2){
    //bottom triangle
    indices.push(i,0,i+2);
    //top triangle
    indices.push(i+1,1,i+3);
    //triangles on side
    indices.push(i,i+1,i+3);
    indices.push(i,i+2,i+3);
  }
  //creating last triangles V0 , Vn , V1
  indices.push(i,0,2);
  indices.push(i+1,1,3);
  indices.push(i,i+1,3);
  indices.push(i,2,3);
  var verticesArray = new Float32Array(vertices);
  var indicesArray = new Uint16Array(indices);
  var colorsArray = new Float32Array(colors);
  // put arrays in gl buffer
  bufferize(gl,verticesArray,colorsArray,indicesArray);
  return indices.length;
}

function drawSphere(gl,color,radius,n_points){
  var i, ai, si, ci;
  var j, aj, sj, cj;
  var p1, p2;
  var vertices = [];
  var indices = [];
  var colors =[];
  // Generate coordinates
  for (j = 0; j <= n_points; j++) {
    //j * Math.PI:  scorro dall'alto al basso la sfera (asse Z)
    aj = j * Math.PI / n_points;
    sj = Math.sin(aj);
    cj = Math.cos(aj);
    for (i = 0; i <= n_points; i++) {
      //i * 2 * Math.PI: per ogni punto dell asse Z traccio una circonferenza
      ai = i * 2 * Math.PI / n_points;
      si = Math.sin(ai);
      ci = Math.cos(ai);
      vertices.push(radius * si * sj,radius *cj,radius *ci * sj)
      colors.push(color.color[0]/255,color.color[1]/255,color.color[2]/255);
    }
  }

  // Generate indices
  for (j = 0; j < n_points; j++) {
    for (i = 0; i < n_points; i++) {
      p1 = j * (n_points+1) + i;
      p2 = p1 + (n_points+1);

      indices.push(p1,p2,p1+1);
      indices.push(p1+1,p2,p2+1);
    }
  }
  var verticesArray = new Float32Array(vertices);
  var indicesArray = new Uint16Array(indices);
  var colorsArray = new Float32Array(colors);
  // put arrays in gl buffer
  bufferize(gl,verticesArray,colorsArray,indicesArray);
  return indices.length;
}

function drawTorus(gl,color,r_piano,r_circle,n_points_circle,n_points_circum){
//function drawTorus(gl,color){
  var i, ai, si, ci;
  var j, aj, sj, cj;
  var p1, p2;
  var vertices = [];
  var indices = [];
  var colors =[];
  var x,y,z;
  // Generate coordinates
  for (i = 0; i <= n_points_circum; i++) {
    theta = i * 2 * Math.PI / n_points_circum;
    sin_theta = Math.sin(theta);
    cos_theta = Math.cos(theta);
    for (j = 0; j <= n_points_circle; j++) {
      phi = j * 2 * Math.PI / n_points_circle;
      sin_phi = Math.sin(phi);
      cos_phi = Math.cos(phi);
      x = (r_piano+r_circle*cos_phi)*cos_theta;
      y = (r_piano+r_circle*cos_phi)*sin_theta;
      z = r_circle*sin_phi;
      vertices.push(x,y,z);
      colors.push(color.color[0]/255,color.color[1]/255,color.color[2]/255);
    }
  }
  //Generate indices
  for (i = 0; i < n_points_circum; i++) {
    for (j = 0; j < n_points_circle; j++) {
      p1 = i * (n_points_circle+1) + j;
      p2 = p1 + (n_points_circle+1);
      indices.push(p1,p2,p1+1);
      indices.push(p1+1,p2,p2+1);
    }
  }
  var verticesArray = new Float32Array(vertices);
  var indicesArray = new Uint16Array(indices);
  var colorsArray = new Float32Array(colors);
  // put arrays in gl buffer
  bufferize(gl,verticesArray,colorsArray,indicesArray);
  return indices.length;
}
//*****************************************************************************
/************************************************************
* BUFFER utility functions                                  *
************************************************************/
function initArrayBuffer(gl, attribute, data, num, type) {
  // Create a buffer object
  var buffer = gl.createBuffer();
  if (!buffer) {
    console.log('Failed to create the buffer object');
    return false;
  }
  // Write date into the buffer object
  gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
  gl.bufferData(gl.ARRAY_BUFFER, data, gl.STATIC_DRAW);
  // Assign the buffer object to the attribute variable
  var a_attribute = gl.getAttribLocation(gl.program, attribute);
  if (a_attribute < 0) {
    console.log('Failed to get the storage location of ' + attribute);
    return false;
  }
  gl.vertexAttribPointer(a_attribute, num, type, false, 0, 0);
  // Enable the assignment of the buffer object to the attribute variable
  gl.enableVertexAttribArray(a_attribute);
  gl.bindBuffer(gl.ARRAY_BUFFER, null);
  return true;
}

function bufferize(gl,vertices,colors,indices){
  // Write the vertex property to buffers (coordinates, colors and normals)
  if (!initArrayBuffer(gl, 'a_Position', vertices, 3, gl.FLOAT)) return -1;
  if (!initArrayBuffer(gl, 'a_Color'   , colors  , 3, gl.FLOAT)) return -1;
  // Unbind the buffer object
  gl.bindBuffer(gl.ARRAY_BUFFER, null);
  // Write the indices to the buffer object
  var indexBuffer = gl.createBuffer();
  if (!indexBuffer) {
    console.log('Failed to create the buffer object');
    return false;
  }
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, indices, gl.STATIC_DRAW);
}
