## Computer graphics

Author      : Moris Pozzati  
Mat number  : 073344  
Description : homework 3  
Date        : 2018/12/21  

### Exercise 1:
Come base per l'esercizio è utilizzato il template fornito in consegna ed utilizzate le funzioni già implementate nell'esercitazione precedente. L'attenzione è posta quindi su due aspetti risultati fondamentali:  
- la creazione dell'array **uvs** contenente la mappatura delle coordinate di texture rispetto ai vertici  
- l'ordine di inserimento degli indici dei vertici **indices** nei solidi in quanto è importante in questo caso definire il fronte ed il retro di ogni triangolo. Per verificare che tutte le facce siano rivolte verso l'esterno viene abilitata l'opzione **gl.CULL_FACE**  
- La texture viene definita in fase di inizializzazione dalla funzione **initTextures** dove a sua volta viene definito l'oggetto *Image* utilizzato per caricare la texture.


### Exercise 2
In fase di inizializzazione viene creato l'oggetto **image** di tipo Image e caricata la texture *03a.jpg*. Per richiedere il caricamento di una nuova texture è sufficiente cambiare il path da cui caricare l'immagine nel dat.GUI. Ad ogni caricamento viene richiamata la funzione (*loadTexture*) associata all'evento *onload* dell'istanza image. La funzione loadTexture passa quindi l'immagine da utilizzare al fragment shader.  
A differenza del primo esercizio è invertito l'ordine di inserimento dei vertici dei triangoli che compongono la semi faccia del cilindro. Viene abilitata l'opzione **gl.CULL_FACE**  e col i tasti freccia è possibile ruotare l'oggetto fino a farlo scomparire (la texture è presente solo sulla faccia interna)  
L'inserimento degli indici dei vertici delle due basi è stato volontariamente omesso. E' sufficiente decommentare le righe 236 e 238 per visualizzare anche queste facce.   


### Exercise 3
Partendo dal primo esercizio è richiesto di implementare il modello di phong per la sola componente diffusa. Nella parte di js è quindi sufficiente calcolare le normali per tutti i vertici delle superfici da passare poi al vertex shader.  
Il modello di Phong viene implementato nel fragment shader.  
Dopo aver implementato il modello si è evidenziato un problema con il canale alpha della texture. Per qualche motivo viene caricato in maniera errata e ciò chè dovrebbe risultare in ombra sembra invece essere colpito da luce diretta. Il tutto è stato risolto forzando a 1 il valore del canale alpha.  
