/******************************************************************************
Author      : Moris Pozzati
Mat number  : 073344
Description : homework 3
Date        : 2018/11/26
*******************************************************************************/

// Vertex shader program
var VSHADER_SOURCE =
  'attribute vec4 a_Position;\n'   +
  'uniform mat4 u_MvpMatrix;\n'    +
  'attribute vec2 a_TexCoord;\n'   +
  'varying vec2 v_TexCoord;\n'     +
  'void main() {\n'                +
  '  gl_Position = u_MvpMatrix * a_Position;\n' +
  '  v_TexCoord = a_TexCoord;\n' +
  '}\n';

// Fragment shader program
var FSHADER_SOURCE =
  '#ifdef GL_ES\n' +
  'precision mediump float;\n' +
  '#endif\n' +
  'uniform sampler2D u_Sampler;\n' +
  'varying vec2 v_TexCoord;\n' +
  'void main() {\n' +
   '  gl_FragColor = texture2D(u_Sampler, v_TexCoord);\n' +
  '}\n';

function main() {
  // Retrieve <canvas> element
  var canvas = document.getElementById('webgl');

 // Get the rendering context for WebGL
  var gl = getWebGLContext(canvas);
  if (!gl) {
    console.log('Failed to get the rendering context for WebGL');
    return;
  }

  // Initialize shaders
  if (!initShaders(gl, VSHADER_SOURCE, FSHADER_SOURCE)) {
    console.log('Failed to intialize shaders.');
    return;
  }

  // Set the vertex coordinates, the color and the normal
  var n = initVertexBuffersCube(gl);
  if (n < 0) {
    console.log('Failed to set the vertex information');
    return;
  }

   // Set texture
  if (!initTextures(gl)) {
    console.log('Failed to intialize the texture.');
    return;
  }

  // Set the clear color and enable the depth test
  gl.clearColor(0.5, 0.5, 0.5, 1);
  gl.enable(gl.DEPTH_TEST);

  // Get the storage locations of uniform variables and so on
  var u_MvpMatrix     = gl.getUniformLocation(gl.program, 'u_MvpMatrix');

  // var u_CameraPos     = gl.getUniformLocation(gl.program, 'u_CameraPos');
  if (!u_MvpMatrix ) {
    console.log('Failed to get the storage location');
    return;
  }
  // *******************************************************************************************
  var cameraPos = [1,3,8];          // camera position
  //********************************************************************************************

  //********************************************************************************************
  // creo una GUI con dat.gui
  var gui = new dat.GUI();
  // checkbox geometry
  var geometria = {cube:true,cone:false,cylinder:false,sphere:false,torus:false};
  //
  gui.add(geometria,'cube').onFinishChange(function(value) {
    // Fires when a controller loses focus.
	  if(value == true){
  		geometria.cube = value;
  		geometria.cone = false;
  		geometria.cylinder = false;
  		geometria.sphere = false;
  		geometria.torus = false;
  		n = initVertexBuffersCube(gl);
  		if (n < 0) {
  			console.log('Failed to set the vertex information');
  			return;
  		}
	  }
    // Iterate over all controllers
    for (var i in gui.__controllers) {
      gui.__controllers[i].updateDisplay();
    }
  });
  gui.add(geometria,'cone').onFinishChange(function(value) {
    // Fires when a controller loses focus.
    if(value == true){
  		geometria.cube = false;
  		geometria.cone = value;
  		geometria.cylinder = false;
  		geometria.sphere = false;
  		geometria.torus = false;
      n = initVertexBuffersCone(gl);
    }
	  // Iterate over all controllers
    for (var i in gui.__controllers) {
      gui.__controllers[i].updateDisplay();
    }
  });
  gui.add(geometria,'cylinder').onFinishChange(function(value) {
    // Fires when a controller loses focus.
  	if(value == true){
  		geometria.cube = false;
  		geometria.cone = false;
  		geometria.cylinder = value;
  		geometria.sphere = false;
  		geometria.torus = false;
      n = initVertexBuffersCylinder(gl);
    }
	  // Iterate over all controllers
    for (var i in gui.__controllers) {
      gui.__controllers[i].updateDisplay();
    }
  });
  gui.add(geometria,'sphere').onFinishChange(function(value) {
    // Fires when a controller loses focus.
	  if(value == true){
  		geometria.cube = false;
  		geometria.cone = false;
  		geometria.cylinder = false;
  		geometria.sphere = value;
  		geometria.torus = false;
  		n = initVertexBuffersSphere(gl);
    }
	  // Iterate over all controllers
    for (var i in gui.__controllers) {
      gui.__controllers[i].updateDisplay();
    }
  });
  gui.add(geometria,'torus').onFinishChange(function(value) {
    // Fires when a controller loses focus.
    if(value == true){
  		geometria.cube = false;
  		geometria.cone = false;
  		geometria.cylinder = false;
  		geometria.sphere = false;
  		geometria.torus = value;
      n = initVertexBuffersTorus(gl);
    }
	  // Iterate over all controllers
    for (var i in gui.__controllers) {
      gui.__controllers[i].updateDisplay();
    }
  });
  //*********************************************************************************************

  var currentAngle = 0.0;           // Current rotation angle
  var vpMatrix = new Matrix4();   // View projection matrix

  // Calculate the view projection matrix
  vpMatrix.setPerspective(30, canvas.width/canvas.height, 1, 100);
  vpMatrix.lookAt(cameraPos[0],cameraPos[1],cameraPos[2], 0, 0, 0, 0, 1, 0);

  var modelMatrix = new Matrix4();  // Model matrix
  var mvpMatrix = new Matrix4(); 　  // Model view projection matrix
  var normalMatrix = new Matrix4(); // Transformation matrix for normals
  // Rotation angle (degrees/second)
  var ANGLE_STEP = 10.0;
  // Last time that this function was called
  var g_last = Date.now();
  function animate(angle) {
    // Calculate the elapsed time
    var now = Date.now();
    var elapsed = now - g_last;
    g_last = now;
    // Update the current rotation angle (adjusted by the elapsed time)
    var newAngle = angle + (ANGLE_STEP * elapsed) / 1000.0;
    return newAngle %= 360;
  }
  var tick = function() {

    /* visualizzazione a pieno schermo */
    //rimuove l'effetto pixelato
    resize(gl.canvas);
    //e aggiusto la vista
    gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
    // Calculate the view projection matrix
    vpMatrix.setPerspective(30, canvas.width/canvas.height, 1, 100);
    vpMatrix.lookAt(cameraPos[0],cameraPos[1],cameraPos[2], 0, 0, 0, 0, 1, 0);

    //enable CULL FACE to see the correct face of texture
    gl.enable(gl.CULL_FACE);
    currentAngle = animate(currentAngle);  // Update the rotation angle

    // Calculate the model matrix
    modelMatrix.setRotate(currentAngle, 0, 1, 0); // Rotate around the y-axis

    mvpMatrix.set(vpMatrix).multiply(modelMatrix);
    // Pass the model view projection matrix to u_MvpMatrix
    gl.uniformMatrix4fv(u_MvpMatrix, false, mvpMatrix.elements);

    // Clear color and depth buffer
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    // Draw the cube(Note that the 3rd argument is the gl.UNSIGNED_SHORT)
    gl.drawElements(gl.TRIANGLES, n, gl.UNSIGNED_SHORT, 0);

    requestAnimationFrame(tick, canvas); // Request that the browser ?calls tick
    };
  tick();
}

//******************************************************************************
// Draw solid functions

function initVertexBuffersCube(gl) {
  // Create a cube
  //    v6----- v5
  //   /|      /|
  //  v1------v0|
  //  | |     | |
  //  | |v7---|-|v4
  //  |/      |/
  //  v2------v3
  // Coordinates
  var positions = new Float32Array([
     1.0, 1.0, 1.0,  -1.0, 1.0, 1.0,  -1.0,-1.0, 1.0,   1.0,-1.0, 1.0, // v0-v1-v2-v3 front
     1.0, 1.0, 1.0,   1.0,-1.0, 1.0,   1.0,-1.0,-1.0,   1.0, 1.0,-1.0, // v0-v3-v4-v5 right
     1.0, 1.0, 1.0,   1.0, 1.0,-1.0,  -1.0, 1.0,-1.0,  -1.0, 1.0, 1.0, // v0-v5-v6-v1 up
    -1.0, 1.0, 1.0,  -1.0, 1.0,-1.0,  -1.0,-1.0,-1.0,  -1.0,-1.0, 1.0, // v1-v6-v7-v2 left
    -1.0,-1.0,-1.0,   1.0,-1.0,-1.0,   1.0,-1.0, 1.0,  -1.0,-1.0, 1.0, // v7-v4-v3-v2 down
     1.0,-1.0,-1.0,  -1.0,-1.0,-1.0,  -1.0, 1.0,-1.0,   1.0, 1.0,-1.0  // v4-v7-v6-v5 back
  ]);

  // TexCoord
  var uvs = new Float32Array([
    1.0, 1.0,   0.0, 1.0,   0.0, 0.0,   1.0, 0.0,  // v0-v1-v2-v3 front
    0.0, 1.0,   0.0, 0.0,   1.0, 0.0,   1.0, 1.0,  // v0-v3-v4-v5 right
    1.0, 0.0,   1.0, 1.0,   0.0, 1.0,   0.0, 0.0,  // v0-v5-v6-v1 up
    1.0, 1.0,   0.0, 1.0,   0.0, 0.0,   1.0, 0.0,  // v1-v6-v7-v2 left
    0.0, 0.0,   1.0, 0.0,   1.0, 1.0,   0.0, 1.0,  // v7-v4-v3-v2 down
    0.0, 0.0,   1.0, 0.0,   1.0, 1.0,   0.0, 1.0   // v4-v7-v6-v5 back
  ]);

  // Indices of the vertices
  var indices = new Uint16Array([
     0, 1, 2,   0, 2, 3,    // front
     4, 5, 6,   4, 6, 7,    // right
     8, 9,10,   8,10,11,    // up
    12,13,14,  12,14,15,    // left
    16,17,18,  16,18,19,    // down
    20,21,22,  20,22,23     // back
 ]);

  // Write the vertex property to buffers (coordinates and normals)
  // Same data can be used for vertex and normal
  // In order to make it intelligible, another buffer is prepared separately
  bufferize(gl,new Float32Array(positions),new Float32Array(uvs),new Uint16Array(indices));
  return indices.length;
}

function initVertexBuffersSphere(gl) { // Create a sphere
  var radius = 1;
  var n_points = 20;
  var i, ai, si, ci;
  var j, aj, sj, cj;
  var p1, p2;
  var vertices = [];
  var indices = [];
  var uvs =[];
  // Generate coordinates
  for (j = 0; j <= n_points; j++) {
    //j * Math.PI:  scorro dall'alto al basso la sfera (asse Z)
    aj = j * Math.PI / n_points;
    sj = Math.sin(aj);
    cj = Math.cos(aj);
    for (i = 0; i <= n_points; i++) {
      //i * 2 * Math.PI: per ogni punto dell asse Z traccio una circonferenza
      ai = i * 2 * Math.PI / n_points;
      si = Math.sin(ai);
      ci = Math.cos(ai);
      vertices.push(radius * si * sj,radius *cj,radius *ci * sj);
      // inserisco le coordinate di texture
      uvs.push(ai/(2 * Math.PI),1-aj/( Math.PI));
    }
  }
  // Generate indices
  for (j = 0; j < n_points; j++) {
    for (i = 0; i < n_points; i++) {
      p1 = j * (n_points+1) + i;
      p2 = p1 + (n_points+1);
      indices.push(p1,p2,p1+1);
      indices.push(p1+1,p2,p2+1);
    }
  }
  var verticesArray = new Float32Array(vertices);
  var indicesArray = new Uint16Array(indices);
  var uvsArray = new Float32Array(uvs);
  // put arrays in gl buffer
  bufferize(gl,verticesArray,uvsArray,indicesArray);
  return indices.length;
}

function initVertexBuffersCylinder(gl) { // Create a cylinder
  var height=2;
  var n_points=40;
  var step = 2*Math.PI / n_points;
  var i = 0;
  var vertices = [];
  var indices = [];
  var uvs = [];
  for(i = 0; i<=Math.PI*2;i+=step){
    // top
    vertices.push(0.0,height/2,0.0);
    vertices.push( Math.cos(i+step),height/2, Math.sin(i+step));
    vertices.push( Math.cos(i),height/2, Math.sin(i));
    // inserisco le coordinate di texture: metto il segno negativo su y per visualizzare la texture nel modo corretto
    uvs.push(0.5,                     0.5,
             0.5-Math.cos(i+step)/2,  0.5+Math.sin(i+step)/2,
             0.5-Math.cos(i)/2,       0.5+Math.sin(i)/2);

    // bottom
    vertices.push(0.0,-height/2,0.0)
    vertices.push( Math.cos(i),-height/2, Math.sin(i));
    vertices.push( Math.cos(i+step),-height/2, Math.sin(i+step));
    // inserisco le coordinate di texture
    uvs.push(0.5,                     0.5,
             0.5+Math.cos(i)/2,       0.5+Math.sin(i)/2,
             0.5+Math.cos(i+step)/2,  0.5+Math.sin(i+step)/2);

    // vertices on side
    vertices.push( Math.cos(i),-height/2, Math.sin(i));
    vertices.push( Math.cos(i+step),-height/2, Math.sin(i+step));
    vertices.push( Math.cos(i+step),height/2, Math.sin(i+step));
    vertices.push(Math.cos(i),height/2, Math.sin(i));
    // inserisco le coordinate di texture
    uvs.push(1-i/(Math.PI*2),         0,
             1-(i+step)/(Math.PI*2),  0,
             1-(i+step)/(Math.PI*2),  1,
             1-i/(Math.PI*2),         1);

  }
  //create indices array
  for(i=0;i<=vertices.length/3-11;i=i+10){
    //top triangle
    indices.push(i,i+1,i+2);
    //bottom triangle
    indices.push(i+3,i+4,i+5);
    //triangles on side
    // inserisco nell'ordine inverso (876 anzichè 679) per determinare il corretto verso della texture
    indices.push(i+8,i+7,i+6);
    // inserisco nell'ordine inverso
    indices.push(i+6,i+9,i+8);
  }
  var verticesArray = new Float32Array(vertices);
  var indicesArray = new Uint16Array(indices);
  var uvsArray = new Float32Array(uvs);
  // put arrays in gl buffer
  bufferize(gl,verticesArray,uvsArray,indicesArray);
  return indices.length;
}

function initVertexBuffersCone(gl) { // Create a cone
  var height=2;
  var n_points=81;
  var step = 2*Math.PI / n_points;
  var i = 0;
  var vertices = [];
  var indices = [];
  var uvs = [];
  for(i = 0; i<=Math.PI*2;i+=step){
    // bottom
    vertices.push(0.0,-height/2,0.0)
    vertices.push( Math.cos(i),-height/2, Math.sin(i));
    vertices.push( Math.cos(i+step),-height/2, Math.sin(i+step));
    // inserisco le coordinate di texture
    uvs.push(0.5,                     0.5,
             0.5+Math.cos(i)/2,       0.5+Math.sin(i)/2,
             0.5+Math.cos(i+step)/2,  0.5+Math.sin(i+step)/2);

    // vertices on side
    vertices.push(0.0,height/2,0.0)
    vertices.push( Math.cos(i),-height/2, Math.sin(i));
    vertices.push( Math.cos(i+step),-height/2, Math.sin(i+step));
    // inserisco le coordinate di texture
    uvs.push(1-(i+step/2)/(Math.PI*2),1,
             1-(i)/(Math.PI*2),       0,
             1-(i+step)/(Math.PI*2),  0);
  }
  //create indices array
  for(i=0;i<=vertices.length/3-7;i=i+6){
    //base triangle
    indices.push(i,i+1,i+2);
    //triangles on side
    indices.push(i+5,i+4,i+3);
  }
  var verticesArray = new Float32Array(vertices);
  var indicesArray = new Uint16Array(indices);
  var uvsArray = new Float32Array(uvs);
  // put arrays in gl buffer
  bufferize(gl,verticesArray,uvsArray,indicesArray);
  return indices.length;
}

function initVertexBuffersTorus(gl) { // Create a torus
  var r_piano= 1;
  var r_circle= 0.5;
  var n_points_circle = 20;
  var n_points_circum = 40;
  var i, ai, si, ci;
  var j, aj, sj, cj;
  var p1, p2;
  var vertices = [];
  var indices = [];
  var uvs =[];
  var x,y,z;
  // Generate coordinates
  for (i = 0; i <= n_points_circum; i++) {
    theta = i * 2 * Math.PI / n_points_circum;
    sin_theta = Math.sin(theta);
    cos_theta = Math.cos(theta);
    for (j = 0; j <= n_points_circle; j++) {
      phi = j * 2 * Math.PI / n_points_circle;
      sin_phi = Math.sin(phi);
      cos_phi = Math.cos(phi);
      x = (r_piano+r_circle*cos_phi)*cos_theta;
      y = (r_piano+r_circle*cos_phi)*sin_theta;
      z = r_circle*sin_phi;
      vertices.push(x,y,z);
      // inserisco le coordinate di texture
      uvs.push(theta/ (2 * Math.PI),phi/ (2 * Math.PI));
    }
  }
  //Generate indices
  for (i = 0; i < n_points_circum; i++) {
    for (j = 0; j < n_points_circle; j++) {
      p1 = i * (n_points_circle+1) + j;
      p2 = p1 + (n_points_circle+1);
      indices.push(p1,p2,p1+1);
      indices.push(p1+1,p2,p2+1);
    }
  }
  var verticesArray = new Float32Array(vertices);
  var indicesArray = new Uint16Array(indices);
  var uvsArray = new Float32Array(uvs);
  // put arrays in gl buffer
  bufferize(gl,verticesArray,uvsArray,indicesArray);
  return indices.length;
}

//*****************************************************************************
/************************************************************
* BUFFER and TEXTURE utility functions                      *
************************************************************/
function bufferize(gl,vertices,uvs,indices){
  // Write the vertex property to buffers (coordinates, colors and uvs)
  if (!initArrayBuffer(gl, 'a_Position', vertices, 3, gl.FLOAT)) return -1;
  if (!initArrayBuffer(gl, 'a_TexCoord'   , uvs  , 2, gl.FLOAT)) return -1;
  // Unbind the buffer object
  gl.bindBuffer(gl.ARRAY_BUFFER, null);
  // Write the indices to the buffer object
  var indexBuffer = gl.createBuffer();
  if (!indexBuffer) {
    console.log('Failed to create the buffer object');
    return false;
  }
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, indices, gl.STATIC_DRAW);
}

function initArrayBuffer(gl, attribute, data, num, type ) {
  // Create a buffer object
  var buffer = gl.createBuffer();
  if (!buffer) {
    console.log('Failed to create the buffer object');
    return false;
  }
  // Write date into the buffer object
  gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
  gl.bufferData(gl.ARRAY_BUFFER, data, gl.STATIC_DRAW);
  // Assign the buffer object to the attribute variable
  var a_attribute = gl.getAttribLocation(gl.program, attribute);
  if (a_attribute < 0) {
    console.log('Failed to get the storage location of ' + attribute);
    return false;
  }
  gl.vertexAttribPointer(a_attribute, num, type, false, 0, 0);
  // Enable the assignment of the buffer object to the attribute variable
  gl.enableVertexAttribArray(a_attribute);

  gl.bindBuffer(gl.ARRAY_BUFFER, null);

  return true;
}

function initTextures(gl) {
  var texture = gl.createTexture();   // Create a texture object
  if (!texture) {
    console.log('Failed to create the texture object');
    return false;
  }

  // Get the storage location of u_Sampler
  var u_Sampler = gl.getUniformLocation(gl.program, 'u_Sampler');
  if (!u_Sampler) {
    console.log('Failed to create the Sampler object');
    return false;
  }
  var image = new Image();  // Create the image object
  if (!image) {
    console.log('Failed to create the image object');
    return false;
  }
  // Register the event handler to be called on loading an image
  image.onload = function(){ loadTexture(gl, texture, u_Sampler, image); };
  // Tell the browser to load an image
  image.src = './textures/ash_uvgrid01.jpg';

  return true;
}

function loadTexture(gl, texture, u_Sampler, image) {
  gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1); // Flip the image's y axis
  // Enable texture unit0
  gl.activeTexture(gl.TEXTURE0);
  // Bind the texture object to the target
  gl.bindTexture(gl.TEXTURE_2D, texture);

  // Set the texture parameters
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
  // Set the texture image
  gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGB, gl.RGB, gl.UNSIGNED_BYTE, image);

  // Set the texture unit 0 to the sampler
  gl.uniform1i(u_Sampler, 0);

  gl.clear(gl.COLOR_BUFFER_BIT);   // Clear <canvas>
}

function resize(canvas) {
  // Lookup the size the browser is displaying the canvas.
  var displayWidth  = canvas.clientWidth;
  var displayHeight = canvas.clientHeight;

  // Check if the canvas is not the same size.
  if (canvas.width  != displayWidth ||
      canvas.height != displayHeight) {

    // Make the canvas the same size
    canvas.width  = displayWidth;
    canvas.height = displayHeight;
  }
}
